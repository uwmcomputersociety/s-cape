function create_demo ()
{
    this.add.image(400, 300, 'sky');
    this.add.image(400, 100, 'logo');

    this.matter.world.setBounds(0, 0, 900, 700, 10, true, true, true, true);
    this.add.image(400, 200, 'brick');
    var particles = this.add.particles('triforce');

    var emitter = particles.createEmitter({
        speed: 100,
        scale: { start: 1, end: 0 },
        blendMode: 'NORMAL'
    });

    var triforce = this.physics.add.image(58, 50, 'triforce');

    triforce.setVelocity(100, 200);
    triforce.setBounce(1, 1);
    triforce.setCollideWorldBounds(true);

    emitter.startFollow(triforce);
}

function create_dungeon ()
{
    this.add.image(400, 300, 'test');
    //var path = '0 307 0 67 8 55 12 53 57 128 86 94 128 136 148 103 190 159 210 135 222 149 248 109 267 133 293 93 321 128 361 75 381 97 439 4 523 117 551 78 563 92 569 93 603 38 637 99 654 53 701 154 729 109 750 140 800 66 800 307';

    this.matter.world.setBounds(0, 0, 900, 700, 10, true, true, true, true);
}


function create_outdoors ()
{
    this.add.image(400, 300, 'outdoor-map');
    this.matter.world.setBounds(0, 0, 900, 700, 10, true, true, true, true);
}
