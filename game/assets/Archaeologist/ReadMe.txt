Section 1: Licensing & About
Section 2: How To Use




//SECTION 1
//Licensing & About
Thanks for supporting the VIEWPOINT Overhead Fantasy Archaeologist assets by LUNARSIGNALS. VIEWPOINT is a new series of pixel art I am creating. Be on the lookout for more packs under this name.

VIEWPOINT Overhead Fantasy Archaeologist is licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). This license allows adaptations of your work to be shared under the same license, and allows your work to be used commercially, as long as it's attributed to LUNARSIGNALS. This is a Free Culture License. https://creativecommons.org/licenses/by-sa/4.0/

LUNARSIGNALS is a one person game development studio making pixel art and independent games. Follow @LUNARSIGNALS on twitter. And find my other products on https://lunarsignals.itch.io/




//SECTION 2
//How To Use
VIEWPOINT Overhead Fantasy Archaeologist comes with *.PNG pixel art files for:

Archaologist with hat, with long hair, and with a buzzcut. It also comes with tan and green military characters with and without jackets. And it also, also comes with a naked buzzcut character.

The animations are the same for all files.

Row 1: Facing Down
Row 2: Facing Right
Row 3: Facing Up

Column 1-4: Walking
Column 5-8: Stab
Column 9-12: Horizontal Swing
Column 13-16: Vertical Swing
Column 17-20: Swimming
Column 21-22: Jumping
Column 25: Hurt

Row 4: Holding up an item/Celebrating