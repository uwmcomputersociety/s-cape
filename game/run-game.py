#!/usr/bin/env python3

# Starts a simple Python webserver in the current directory.
# Not for production use!
import http.server
import socketserver

PORT = 8000
server_address = '127.0.0.1'

Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer((server_address, PORT), Handler) as httpd:
    print("Serving s-cape at (http://" + server_address + ":" + str(PORT) + ")")
    httpd.serve_forever()
