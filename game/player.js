function create_player ()
{
    player = this.matter.add.sprite(100, 100, 'char-firefox');
    player.setBounce(.2);
    player.setMass(100);

    this.anims.create({
        key: 'walk-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 8, end: 11 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-left',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 12, end: 15 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-up',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 32, end: 39 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-down',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 56, end: 59 }),
        frameRate: 8,
        repeat: -1
    });

    this.anims.create({
        key: 'dance-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 16, end: 20 }),
        frameRate: 8,
        repeat: -1
    });

    this.anims.create({
        key: 'attack-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 0, end: 4 }),
        frameRate: 8,
        repeat: 1
    });
}

function update_player ()
{
    if (cursors.up.isDown)
    {
        player.setVelocityY(-2.5);
        player.anims.play('walk-up', true);
        playerDirection = 'N';
    }
    else if (cursors.down.isDown)
    {
        player.setVelocityY(2.5)
        player.anims.play('walk-down', true);
        playerDirection = 'S';
    }
    else {
	player.setVelocityY(0);
    }

    if (cursors.left.isDown)
    {
        player.setVelocityX(-2.5);
        player.anims.play('walk-left', true);
        playerDirection = 'W';
    }
    else if (cursors.right.isDown)
    {
        player.setVelocityX(2.5)
        player.anims.play('walk-right', true);
        playerDirection = 'E';
    }
    else
    {
	    player.setVelocityX(0);
    }

    if (!cursors.left.isDown && !cursors.right.isDown && !cursors.up.isDown && !cursors.down.isDown) {
        switch (playerDirection) {
        case 'N':
            player.anims.play('walk-up', false);
            break;
        case 'E':
            player.anims.play('walk-right', false);
            break;
        case 'S':
            player.anims.play('walk-down', false);
            break;
        case 'W':
            player.anims.play('walk-left', false);
            break;
        }
    }

    player.setAngle(0);
}