var config = {
    type: Phaser.AUTO,
    name: "s-cape",
    width: 800,
    height: 640,
    physics: {
        default: 'matter',
        matter: {
            gravity: { y: 0, x: 0 }
        },
	debug: {
	    showBody: true,
	    showStaticBody: true
	}
    },
    scene: {
        preload: preload,
        create: create,
	update: update
    },
};

var game = new Phaser.Game(config);
var player;
var playerDirection = 'S';
var cursors;

// preload game assets - runs once at start
function preload ()
{
    // this.load.setBaseURL('http://labs.phaser.io');

    this.load.image('sky', 'assets/space.jpg');
    this.load.image('logo', 'assets/logo.png');
    this.load.image('triforce', 'assets/triforce.png');
    this.load.image('test', 'assets/dungeon_pre.png');
    this.load.spritesheet('char-firefox','assets/firefox.png', {frameWidth: 96, frameHeight: 96});
    this.load.audio('theme', [
        'assets/temp.ogg',
        'assets/temp.mp3'
    ]);

    // Add map
    this.load.image('tiles', 'assets/tilesets/floor-tiles-large.png');
    this.load.tilemapTiledJSON('outdoor-map', 'assets/tilemaps/outdoor.json');
}

// create game world - runs once after "preload" finished
function create ()
{
    // Environment
    // this.add.image(400, 300, 'test');
    // //var path = '0 307 0 67 8 55 12 53 57 128 86 94 128 136 148 103 190 159 210 135 222 149 248 109 267 133 293 93 321 128 361 75 381 97 439 4 523 117 551 78 563 92 569 93 603 38 637 99 654 53 701 154 729 109 750 140 800 66 800 307';

    // this.matter.world.setBounds(0, 0, 900, 700, 10, true, true, true, true);

    
    // Add map
    const map = this.make.tilemap({key: 'outdoor-map', tileWidth: 32, tileWidth: 32});

    // First parameter is the name of your tileset in Tiled.
    const tileset = map.addTilesetImage('floor', 'tiles');

    // First parameter is the name of your layer in Tiled.
    const layer = map.createStaticLayer('floor', tileset, 0, 0);


    // Player
    player = this.matter.add.sprite(100, 100, 'char-firefox');
    player.setBounce(.2);
    player.setMass(100);

    this.anims.create({
        key: 'walk-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 8, end: 11 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-left',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 12, end: 15 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-up',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 32, end: 39 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'walk-down',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 56, end: 59 }),
        frameRate: 8,
        repeat: -1
    });

    this.anims.create({
        key: 'dance-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 16, end: 20 }),
        frameRate: 8,
        repeat: -1
    });

    this.anims.create({
        key: 'attack-right',
        frames: this.anims.generateFrameNumbers('char-firefox', { start: 0, end: 4 }),
        frameRate: 8,
        repeat: 1
    });


    // // Music
    var music = this.sound.add("theme");

    // I do not think this is the correct way to make it loop,
    // but it works for now.
    music.loop = true;
    music.play();


    cursors = this.input.keyboard.createCursorKeys();
}

// update gameplay - runs in continuous loop after "create" finished
function update() {
    update_player();
}
